import { Component, OnInit } from '@angular/core';
import { ImageService } from '../shared/images.service';

@Component({
  selector: 'app-image-list',
  templateUrl: './image-list.component.html',
  styleUrls: ['./image-list.component.css']
})
export class ImageListComponent implements OnInit {
  images: any[];
  searchQuery: any;
  imagesFound: boolean = false;
  searching: boolean = false;

  handleSuccess(data) {
    this.imagesFound = true;
    this.images = data.hits;
    console.log(data);
  }

  handleError(error) {
    console.log(error);
  }


  constructor(private _imageService: ImageService) { }

   searchImages(query: string) {
    /*return this._imageService.getImage(query).subscribe(
      data => console.log(data),
      error => console.log(error),
      () => console.log("Request Complete")
    );*/

    /*For request in process*/
     this.searching = true;
     return this._imageService.getImage(query).subscribe(
      data => this.handleSuccess(data),
      error => this.handleError(error),
      () => this.searching = false
    );
   }

  ngOnInit() {
  }

}
