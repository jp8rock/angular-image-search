import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';



import { AppComponent } from './app.component';
import { ImageListComponent } from './image-list/image-list.component';

import { ImageService } from './shared/images.service';

import { MaterialModule } from './material.module';
import { MasonryModule } from 'angular2-masonry/angular2-masonry';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';


@NgModule({
  declarations: [
    AppComponent,
    ImageListComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    MasonryModule,
    BrowserAnimationsModule,
    MaterialModule

  ],
  providers: [ImageService],
  bootstrap: [AppComponent]
})
export class AppModule { }
